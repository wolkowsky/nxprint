﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace NXPrint
{
    public partial class Form1 : Form
    {
        private int _topMargin = 10;
        private int _leftMargin = 20;
        private int zrodlo = Int32.Parse(ConfigurationManager.AppSettings["idZrodla"]);
        private int wydruk = Int32.Parse(ConfigurationManager.AppSettings["idWydruku"]);
        private int format = Int32.Parse(ConfigurationManager.AppSettings["idFormatu"]);
        private readonly int _gidTyp;
        private readonly int _gidNumer;
        private readonly int _gidLp;

        private List<Tuple<int, string>> osoby;

        public Form1()
        {
            InitializeComponent();

            var gids = Helpers.WczytajParamXL(Environment.GetCommandLineArgs()); //1886; //

            _gidTyp = gids.Item3;
            _gidNumer = gids.Item1;
            _gidLp = gids.Item2;

            osoby = Helpers.GetPersons();
            cbOsoba.DataSource = osoby.Select(x => x.Item2).ToList();

            //_gidNumer = 110375;
            //_gidLp = 1;

            var dokumenty = Helpers.GetDok(_gidNumer);

            Label lblNazwaElem = new Label();
            lblNazwaElem.AutoSize = true;
            lblNazwaElem.Text = dokumenty.Elementy[_gidLp - 1].Towar.TwrKod;
            lblNazwaElem.Top = _topMargin;
            lblNazwaElem.ForeColor = Color.DarkGreen;
            _topMargin = _topMargin + 20;

            panelGlowny.Controls.Add(lblNazwaElem);

            for (int j = 0; j < dokumenty.Elementy[_gidLp - 1].SubPozycje.Count; j++)
            {
                Label cbVin = new Label();
                cbVin.AutoSize = true;
                cbVin.Text = dokumenty.Elementy[_gidLp - 1].SubPozycje[j].Cecha;
                cbVin.Top = _topMargin;
                cbVin.ForeColor = Color.Blue;
                cbVin.Font = new Font("Arial", 12.0f);
                _topMargin = _topMargin + 25;
                panelGlowny.Controls.Add(cbVin);

                var kategorie = Helpers.GetAtributes(_gidNumer, dokumenty.Elementy[_gidLp - 1].GIDLp.Value);
                if (kategorie.Count == 0)
                {
                    Label txtVin = new Label();
                    txtVin.AutoSize = true;
                    txtVin.Text = @"Brak wybranego atrybutu na pozycji.";
                    txtVin.ForeColor = Color.Red;
                    txtVin.Top = _topMargin;
                    txtVin.Left = _leftMargin;
                    panelGlowny.Controls.Add(txtVin);
                }
                for (int k = 0; k < kategorie.Count; k++)
                {
                    CheckBox cbTyp = new CheckBox();
                    cbTyp.Name = dokumenty.Elementy[_gidLp - 1].SubPozycje[j].Cecha + j + k;
                    cbTyp.AutoSize = true;
                    cbTyp.Checked = false;
                    cbTyp.Text = kategorie[k];
                    cbTyp.Top = _topMargin;
                    cbTyp.Left = _leftMargin;
                    _leftMargin = _leftMargin + cbTyp.Width;
                    panelGlowny.Controls.Add(cbTyp);
                }
                _leftMargin = 20;
                _topMargin = _topMargin + 20;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var choosen = cbOsoba.SelectedItem.ToString();

                if (!String.IsNullOrWhiteSpace(choosen))
                {
                    var wybranaOsoba = osoby.First(x => x.Item2 == choosen);
                    Helpers.InsertCoc(_gidTyp, _gidNumer, _gidLp, wybranaOsoba.Item1, wybranaOsoba.Item2);
                }
                else
                {
                    MessageBox.Show(@"Nie wybrano osoby podpisującej.");
                    return;
                }

                var controls = GetAll(this, typeof(CheckBox));
                List<String> filtr = new List<string>();

                int isAnyChecked = 0;


                foreach (var con in controls)
                {
                    var cb = (CheckBox)con;
                    if (cb.Checked)
                    {
                        isAnyChecked++;
                        filtr.Add($"(Dst_Cecha = '{cb.Name.TruncateLongString(17)}' AND AtH_Wartosc = '{cb.Text}')");
                    }
                }

                if (isAnyChecked > 0)
                {

                    String filtrSql = String.Format("(TraElem.TrE_GIDTyp={0} AND  TrE_GIDFirma=1124623 AND TraElem.TrE_GIDNumer={1}) AND (",_gidTyp, _gidNumer);

                    for (int i = 0; i < filtr.Count; i++)
                    {
                        if (i < filtr.Count - 1)
                            filtrSql += $"{filtr[i]} OR ";
                        else
                        {
                            filtrSql += $"{filtr[i]})";
                        }
                    }

                    string dokNumer = TaskProcessor.Plugin.ERPXL.Plugin.NarzedziaPobierzNumerDok(_gidTyp, _gidNumer, ConfigurationManager.AppSettings["connStr"]);

                    int idsesji = TaskProcessor.Plugin.ERPXL.Plugin.SesjaXLUtworz(new Guid(),
                        ConfigurationManager.AppSettings["Ope"], ConfigurationManager.AppSettings["Haslo"],
                        ConfigurationManager.AppSettings["Baza"]);

                    var pdf = TaskProcessor.Plugin.ERPXL.Plugin.NarzedziaWykonajWydruk(
                        zrodlo,
                        wydruk,
                        format,
                        filtrSql,
                        ConfigurationManager.AppSettings["connStr"]);


                    TaskProcessor.Plugin.ERPXL.Plugin.SesjaXLZakoncz(idsesji);

                    string filepath = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["FolderPlikow"]);
                                            
                    string fullFilePath = String.Empty;

                    if (!string.IsNullOrEmpty(filepath))
                    {
                        dokNumer = "COC-"+dokNumer.Replace(" ", "_").Replace(":", "").Replace("(", "_").Replace(")", "_").Replace("-", "_").Replace("/", "_") + ".pdf";
                        fullFilePath = Path.Combine(filepath, dokNumer);

                        using (var fileStream = new FileStream(fullFilePath, FileMode.OpenOrCreate))
                        {
                            fileStream.Write(pdf, 0, pdf.Length);
                        }

                        System.Diagnostics.Process.Start($"{fullFilePath}");
                    }
                    else
                    {
                        System.IO.File.WriteAllBytes("file.pdf", pdf);
                        System.Diagnostics.Process.Start(Environment.CurrentDirectory + "\\file.pdf");
                    }
                }
                else
                {
                    MessageBox.Show("Brak zaznaczonych opcji.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($@"Wystąpił błąd. Szczegóły {ex.Message}");
            }
        }

        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var controls = GetAll(this, typeof(CheckBox));

            foreach (var control in controls)
            {
                var x = (CheckBox) control;
                x.Checked = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var controls = GetAll(this, typeof(CheckBox));

            foreach (var control in controls)
            {
                var x = (CheckBox)control;
                x.Checked = false;
            }
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            if (!Helpers.CheckIfTableExists())
            {
                Helpers.CreateTableForPrint();
            }
        }
    }
}
