﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using TaskProcessor.Plugin.ERPXL.Common;

namespace NXPrint
{
    public static class Helpers
    {
        private static readonly string _connStr = ConfigurationManager.AppSettings["connStr"];
        public static Tuple<int,int,int> WczytajParamXL(string[] args)
        {
            var xlArgs = File.ReadAllLines(args[1]);
            var d = xlArgs[0].Replace(",", ";").Split(';');
            return new Tuple<int, int, int>(Int32.Parse(d[0]), Int32.Parse(d[1]), Int32.Parse(d[2]));
        }

        public static XLDokHandlowyNag GetDok(int gidNumer)
        {
            return TaskProcessor.Plugin.ERPXL.Plugin.DokHandlowyWczytaj(gidNumer, _connStr);
        }

        public static List<String> GetAtributes(int gidNumer, int lp)
        {
            List<string> atrList = new List<string>();

            using (var conn = new SqlConnection(_connStr))
            using (var cmd = new SqlCommand(@"
                SELECT 
                    AtH_Wartosc 
                FROM 
                    CDN.Atrybuty
                LEFT JOIN CDN.AtrybutyKlasy
	                ON AtK_ID = Atr_AtkId
                INNER JOIN CDN.AtrybutyHist 
	                ON AtH_ObiNumer = Atr_ObiNumer AND AtH_ObiTyp = Atr_ObiTyp AND AtH_ObiLp = Atr_ObiLp
                 WHERE Atr_ObiNumer = @numer AND Atr_ObiLp = @lp and AtK_Nazwa = 'COC_DMCWybrany'
                ", conn))
            {
                cmd.Parameters.AddWithValue("numer", gidNumer);
                cmd.Parameters.AddWithValue("@lp", lp);

                conn.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        atrList.Add(reader["AtH_Wartosc"].ToString());
                    }
                }
            }


            return atrList;
        }

        public static string TruncateLongString(this string str, int maxLength)
        {
            return str.Substring(0, Math.Min(str.Length, maxLength));
        }

        public static List<Tuple<int, string>> GetPersons()
        {
            List<Tuple<int, string>> personList = new List<Tuple<int, string>>();

            using (var conn = new SqlConnection(_connStr))
            using (var cmd = new SqlCommand(@"
                SELECT id, nazwisko FROM CDN.COCPodpisujacy
                ", conn))
            {
                conn.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        new Tuple<int, string>(Int32.Parse(reader["id"].ToString()), reader["nazwisko"].ToString());
                        personList.Add(new Tuple<int, string>(Int32.Parse(reader["id"].ToString()), reader["nazwisko"].ToString()));
                    }
                }
            }

            return personList;
        }

        public static void InsertCoc(int gidTyp, int gidNumer, int gidLp, int idPodpisujacego, string podpisujacy)
        {
            using (var conn = new SqlConnection(_connStr))
            using (var cmd = new SqlCommand(@"
                Insert into CDN.NXPrint (NXP_GidTyp, NXP_GidNumer, NXP_GidLp, NXP_Wartosc, NXP_WartoscTxt) values (@gidTyp,@gidNumer,@gidLp,@idPod, @podpisujacy)
                ", conn))
            {
                cmd.Parameters.AddWithValue("@gidTyp", gidTyp);
                cmd.Parameters.AddWithValue("@gidNumer", gidNumer);
                cmd.Parameters.AddWithValue("@gidLp", gidLp);
                cmd.Parameters.AddWithValue("@idPod", idPodpisujacego);
                cmd.Parameters.AddWithValue("@podpisujacy", podpisujacy);


                conn.Open();

                cmd.ExecuteScalar();
            }
        }


        public static bool CheckIfTableExists()
        {
            const string sqlSelect = @"IF (EXISTS (SELECT * 
                                 FROM INFORMATION_SCHEMA.TABLES 
                                 WHERE TABLE_SCHEMA = 'CDN' 
                                 AND  TABLE_NAME = 'NXPrint'))
                                    SELECT 1 as 'Exists'
                                ELSE
	                                SELECT 0 as 'Exists'";
            try
            {
                using (SqlConnection conn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cmd = new SqlCommand(sqlSelect, conn))
                    {
                        conn.Open();
                        return (int)cmd.ExecuteScalar() == 1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Błąd danych wejściowych.  Dodatkowe informacje: {ex.Message}");
            }
        }

        public static void CreateTableForPrint()
        {
            const string sqlSelect = @"BEGIN TRANSACTION
                                        SET QUOTED_IDENTIFIER ON
                                        SET ARITHABORT ON
                                        SET NUMERIC_ROUNDABORT OFF
                                        SET CONCAT_NULL_YIELDS_NULL ON
                                        SET ANSI_NULLS ON
                                        SET ANSI_PADDING ON
                                        SET ANSI_WARNINGS ON
                                        COMMIT
                                        BEGIN TRANSACTION
                                        
                                        CREATE TABLE CDN.NXPrint
                                               (
                                               NXP_Id int NOT NULL identity,
                                               NXP_GidTyp int NOT NULL,
                                               NXP_GidNumer int NOT NULL,
                                               NXP_GidLp int NOT NULL,
                                               NXP_Wartosc int NULL,
                                               NXP_WartoscTxt nvarchar(50) NULL,
	                                           NXP_Jezyk int NULL
                                               )  ON [PRIMARY]
                                        
                                        ALTER TABLE CDN.NXPrint ADD CONSTRAINT
                                               PK_Table_NXPrint PRIMARY KEY CLUSTERED 
                                               (
                                               NXP_Id
                                               ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

                                        
                                        ALTER TABLE CDN.NXPrint SET (LOCK_ESCALATION = TABLE)
                                        
                                        COMMIT
                                        ";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cmd = new SqlCommand(sqlSelect, conn))
                    {
                        conn.Open();
                        cmd.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Nie udało się utworzy tabeli. Dodatkowe informacje: {ex.Message}");
            }
        }
    }
}
